var db = require('../models/db')
var model = require('../models/users')
var postModel = require('../models/posts')
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken')
var md5 = require('md5');

const saltRounds = 10;



// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};

exports.usersReadAll = function(req, res){
  model.find(function(err, user){
    if(err) {res.status(404).json(err);}
    response.data = user;
    res.status(200).json(response);
  })
}

exports.editUser = function(req, res){
  if(!req.body.newUser.username || !req.body.newUser.email || !req.body.newUser.password){
    res.status(400).json("All fills are required")
  }
  var regex = /^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/
  if (!regex.test(req.body.newUser.email)){
    response.message="Invalid email";
    res.status(200).json(response);
  }
  else{
    model.findOne({username:req.body.newUser.username}, function(err, user){
      if(err){res.status(404).json({error:"username err"});}
      if(user && user.username != req.body.prevUsername){
        response.message="Username already used";
        return res.status(200).json(response);
      }
      else{
        model.findOne({email:req.body.newUser.email}, function(err, user){
          if(err){res.status(404).json({error:"email err"});}
          if(user && user.username != req.body.prevUsername){
            response.message="Email already used";
            return res.status(200).json(response);
          }
          else{
            var salt = bcrypt.genSaltSync(saltRounds);
            var hashPassword = bcrypt.hashSync(req.body.newUser.password, salt);
            var username = req.body.newUser.username;
            var email = req.body.newUser.email;
            model.findOneAndUpdate({username:req.body.prevUsername}, {$set: {username:username, email:email, password:hashPassword}}, {new:true}, function(err, user){
              if(err){res.status(404).json({error:"err"})}
              response.message="Success";
              response.data = {token:jwt.sign({ username: user.username, _id: user._id}, 'RESTFULAPIs'), hash:md5((user.email.trim()).toLowerCase())};
              updator(req.body.prevUsername, username)
              res.status(200).json(response);
            })
          }
        })
      }
    })
  }
}

var updator = function(prevStuff, newStuff){
  model.update({followedBy: prevStuff}, {$set: {"followedBy.$": newStuff}}, {multi:true}, function(err, user){
    if(err){console.log(err)}
  })
  model.update({blocked: prevStuff}, {$set: {"blocked.$": newStuff}}, {multi:true}, function(err, user){
    if(err){console.log(err)}
  })
  model.update({follow: prevStuff}, {$set: {"follow.$": newStuff}}, {multi:true}, function(err, user){
    if(err){console.log(err)}
  })
  postModel.update({author: prevStuff}, {$set: {author: newStuff}}, {multi:true}, function(err, post){
    if(err){console.log(err)}
  })
}

exports.getFollowers = function(req, res){
  var username = req.url.split("/").slice(2)[0]
  model.find({username:username}, {_id:0, followedBy:1, follow:1}, function(err, user){
    if(err) {res.status(404).json(err);}
    if(user){
      response.data = user[0];
      res.status(200).json(response);
    }
    else{
      res.message = "No followers";
      res.status(200).json(response);
    }
  })
}

exports.block = function(req, res){
  model.update({username:req.body.blocker},
  {$addToSet: {blocked: req.body.blocked}},
  function(err, object) {
    if(err){sendError(err, res);}
    else{
      response.message=req.body.blocked + " has been blocked"
      model.update({username:req.body.blocked},
        {$pull: {follow:req.body.blocker}},
        {},
        function(err, object){
          if(err){
            sendError(err, res);
          }
          else{
            res.status(200).json(response);
          }
        })
    }
  })
  model.update({username:req.body.blocker},
    {$pull: {followedBy: req.body.blocked}},
    {},
    function(err, object) {
      if (err){
        sendError(err, res);
      }
  });
}

exports.unblock = function(req, res){
  model.update({username:req.body.blocker},
  {$pull: {blocked: req.body.blocked}},
  {},
  function(err, object) {
    if(err){sendError(err, res);}
    else{
      response.message = req.body.blocked + " has been unblocked"
      res.status(200).json(response);
    }
  })
}

exports.follow = function(req, res){
  model.findOne({username:req.body.username}, function(err, user){
    if(user.blocked.find(function(userBlocked){
      return userBlocked == this
    }, req.body.follower)!=undefined)
    {
      response.message = "You've been blocked by this user";
      res.status(200).json(response)
    }
    else{
      model.update({username:req.body.follower},
        {$addToSet: {follow: req.body.username}},
        {},
        function(err, object) {
          if (err){
            console.warn(err.message);  // returns error if no matching object found
          }
          else{
            response.message = "You're now following "+ req.body.username
            res.status(200).json(response);
          }
        });
        model.update({username:req.body.username},
          {$addToSet: {followedBy: req.body.follower}},
          {},
          function(err, object) {
            if (err){
              console.warn(err.message);  // returns error if no matching object found
            }
          });
    }
  })
}

exports.unfollow = function(req, res){
  model.update({username:req.body.follower},
    {$pull: {follow: req.body.username}},
    {},
    function(err, object) {
      if (err){
        console.warn(err.message);  // returns error if no matching object found
      }
      else{
        response.message = "You're not following "+ req.body.username+" anymore"
        res.status(200).json(response);
      }
  });

  model.update({username:req.body.username},
    {$pull: {followedBy: req.body.follower}},
    {},
    function(err, object) {
      if (err){
        console.warn(err.message);  // returns error if no matching object found
      }
  });
}
