var db = require('../models/db')
var model = require('../models/posts')
var userModel = require('../models/users')
var user = require('../models/users')
var decode = require('jwt-decode')

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};
let responseThread = {
    status: 200,
    data: [],
    message: null
};

exports.newPost = function (req, res) {
  var content = req.body[0].content;
  var username = req.body[1]
  if(!content){
    return res.status(400).json({mess:"All fills are required"})
  }
  explode = content.split(" ")
  var newPost = new model (({content :content, author: username}));
  newPost.save(function (err,resl) {
    if (err) {
      return res.status(404).json(err);
    }
     data = { status: false, error_code: 0, message: 'Unable to insert' };
    if (resl) {
      data = { status: true, error_code: 0,result: resl, message: 'Inserted successfully' };
      explode.forEach(function(word){
        if (word.charAt(0)=="#"){
          model.update({content:content},
            {$addToSet: {hashtags: word}},
            {},
            function(err, object) {
              if (err){
                console.warn(err.message);  // returns error if no matching object found
              }
            });
          }
        })
    }
      userModel.update({username:username}, {$inc:{nbrTwits: 1}}, function(err, upTodate){
        if(err){console.log(err)}
      });
      return res.status(200).json(data)
  });
};

exports.deletePost = function(req, res){
   var url = req.url.split("/"),
   id = url.slice(2).toString();
   var token = req.headers.authorization;
   var decoded = decode(token),
   username = decoded.username;
   model.remove({_id:id}, function(err){
    if(err){res.status(404).json({err:"An error occured : "+err});}
    response.data="Yeah bob";
    userModel.update({username:username}, {$inc:{nbrTwits: -1}}, function(err, upTodate){
      if(err){console.log(err)}
    });
    res.status(200).json(response);
  })
}

exports.getUserPosts = function (req, res) {
  if (!req.body.username){
    var url = req.url.split("/"),
    author = url.slice(2).toString();
  }
  else {
    var author = req.body.username;
  }
  model.find({author:author}, ['content', 'author', 'date'], {sort:{date: -1}}, function(err, posts){
    if(err){res.status(404).json({error:err});}
    if(posts != ""){
        response.data = posts
        res.status(200).json(response);
      }
    else{
      response.data = "";
      res.status(200).json(response)
    }
  })
};

exports.getFollowedPosts = function (req, res) {
  response.data=[];
  user.find({username:req.body.username}, {follow: 1, _id:0}, function(err, followed){
    if(err){res.status(404).json({error:err});}
    if(followed && followed[0].follow.length > 0){
      var i = 0;
      followed[0].follow.forEach(function(user){
        model.find({author:user}, ['content', 'author', 'date'], {sort:{date: -1}}, function(err, posts){
          if(err){res.status(404).json({error:err});}
          if(posts != ""){
              response.data.push(posts)
            }
          i+=1;
          if(i === followed[0].follow.length){
            if(posts.length == 0){
              response.message = "Not twit has been posted by the twittos you're following";
              res.status(200).json(response);
            }
            else{
              res.status(200).json(response);
            }
          }
        })
      })
    }
    else{
      response.message = "You're not following anyone yet."
      res.status(200).json(response)
    }
  });
}

exports.getLastPosts = function (req, res) {
  model.find({}, ['content', 'author', 'date'], {sort:{date: -1}}, function(err, posts){
    if(err){res.status(404).json({error:err});}
    if(posts){
        responseThread.data = posts
        res.status(200).json(responseThread);
      }
    else{
      responseThread.data = "No posts yet, be the first to twit !"
      res.status(404).json(responseThread)
    }
  })
};

exports.getPostsByHash = function(req, res){
  var hash = "#"+req.body.hashtag
  model.find({hashtags:hash},
    ['content', 'author', 'date'],
    {sort:{date:-1}},
    function(err, posts){
    if(err){res.status(404).json({error:err});}
    if(posts != ""){
        response.data = posts;
        res.status(200).json(response);
      }
    else{
      res.status(404).json({err:"Nothing found"})
    }
  })
}

exports.getPostById = function(req, res){
  var url = req.url.split("/"),
  id = url.slice(2).toString();

  model.findOne({_id:id}, ['content', 'author'], function(err, post){
    if(err){res.status(404).json({errpr:err});}
    else{
      response.data=post;
      res.status(200).json(response);
    }
  })
}

exports.editPost = function(req, res){
  var content = req.body.content.content;
  var id = req.body.id;
  model.findByIdAndUpdate(id, {$set: {content:content}}, {new:true}, function(err, post){
    if(err){res.status(404).json({error:err})}
    response.data="It works !"
    res.status(200).json(response);
  })
}

// exports.getAllPosts = function(req, res){
//   model.find({}, ['author'], function(err, posts){
//     if(err){res.status(404).json({error:err});}
//     if(posts){
//       console.log(posts);
//       response.data=posts;
//       res.status(200).json(response)
//     }
//   })
// }
