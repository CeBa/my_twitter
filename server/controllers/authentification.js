var db = require('../models/db')
var model = require('../models/users')
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken')
var decode = require('jwt-decode')
var md5 = require('md5');
var nodemailer = require('nodemailer');
const saltRounds = 10;

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};

exports.register = function (req, res) {
  if(!req.body.username || !req.body.email || !req.body.password){
    response.message = "All fills are required"
    res.status(200).json()
  }
  var regex = /^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/
  if (!regex.test(req.body.email)){
    response.message="Invalid email"
    res.status(200).json(response)
  }
  else{
    model.findOne({username:req.body.username}, function(err, user){
      if(err){sendError(err, res)}
      if(user){
        response.message="Username already used"
        return res.status(200).json(response)
      }
      else{
        model.findOne({email:req.body.email}, function(err, user){
          if(err){sendError(err, res)}
          if(user){
            response.message="Email already used"
            return res.status(200).json(response)
          }
          else{
            // sendemail(req.body.email)
            var salt = bcrypt.genSaltSync(saltRounds);
            var hashPassword = bcrypt.hashSync(req.body.password, salt);
            var newUser = new model (({username: req.body.username, email :req.body.email, password: hashPassword}));
            newUser.save(function (err,resl) {
              if (err) {
                response.message = 'Registration failed';
              }
              if (resl) {
                response.message = 'Success'
              }
              res.status(200).json(response)
            });
          }
        })
      }
    })
  }

};

exports.login = function (req, res) {
  if(!req.body.username || !req.body.password){
    return res.status(400).json({mess:"All fills are required"})
  }
  model.findOne({username:req.body.username}, function(err, user){
    if(err){sendError(err, res)}
    if(user){
      var matchPasswords = bcrypt.compareSync(req.body.password, user.password);
      if(matchPasswords){
        response.data = {token:jwt.sign({ username: user.username, _id: user._id}, 'RESTFULAPIs'), hash:md5((user.email.trim()).toLowerCase())};
        response.message = "Success";
        // sendemail(req, res)
        res.status(200).json(response);
      }
      else{
        response.message = "Invalid password"
        res.status(200).json(response)
        // sendError('Invalid password', res);
      }
    }
    else{
      response.message = "Invalid username"
      res.status(200).json(response)
    }
  })
};

exports.loginRequired = function (req, res, next){
  if(req.headers.authorization){
    var token = req.headers.authorization;
    var decoded = decode(token),
    username = decoded.username;
    model.findOne({username:username}, function(err, user){
      if(err){sendError(err, res)}
      if(user){
        next()
      }
      else{
        res.message="Unauthorized user"
        res.status(200).json(response)
      }
    })
  }
  else{
    res.message="Unauthorized user"
    res.status(200).json(response)
  }
}

sendemail = function(email){
	var transporter = nodemailer.createTransport({
		service: 'gmail',
		  auth: {
		    user: 'cedric.badarou@gmail.com',
		    pass: 'beavisetbutthead',
		    host: 'smtp.gmail.com',
		    ssl: true
		  }
	});
	var mailOptions = {
			from: 'cedric.badarou@gmail.com',
			to: "cedric.badarou@gmail.com", //get from form input field of html file
			subject: 'Sending Email using Node.js',
			text: 'That was easy!'
	};
		transporter.sendMail(mailOptions, function(error, info){
		  if (error) {
		    console.log(error);
		  } else {
		    console.log('Email sent: ' + info.response);
		  }
		});
};
