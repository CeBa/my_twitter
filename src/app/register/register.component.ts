import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { User } from './user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css', '../../form.css', '../app.component.css']
})
export class RegisterComponent {

  constructor(private _dataService: DataService,
              private router: Router) {}
  model = new User('', '', '');
  submitted = false;
  conf_pass = "";
  message = "";

  onSubmit() {
    this._dataService.request = this.model
    if(this.conf_pass == this.model.password){
      this._dataService.postUsers()
      .subscribe(res => {
        this.message = res.message;
        if(this.message == "Success"){
          this.router.navigateByUrl('/login')
        }
      });
    }
    else{
      this.message = "The password and its confirmation must be the same. "
    }
  }

}
