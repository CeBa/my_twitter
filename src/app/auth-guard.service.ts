import { Injectable }       from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
}                           from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  loggedIn = false;
  url = "";
  remember = false;

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (localStorage.getItem('username') || sessionStorage.getItem('username')){
      this.loggedIn = true
    }
    this.url = state.url;

    return this.checkLogin();
  }

  checkLogin(): boolean {
    if (this.loggedIn) { return true; }

    this.router.navigate(['/login']);
    return false;
  }

  getCurrentUser(): string{
    if(localStorage.getItem('username')){
      return(localStorage.getItem('username'))
    }
    else if(sessionStorage.getItem('username')){
      return(sessionStorage.getItem('username'))
    }
  }

  getHash(): string{
    if(localStorage.getItem('hash')){
      return(localStorage.getItem('hash'))
    }
    else if(sessionStorage.getItem('hash')){
      return(sessionStorage.getItem('hash'))
    }
  }

  getToken(): string{
    if(localStorage.getItem('token')){
      return(localStorage.getItem('token'))
    }
    else if(sessionStorage.getItem('token')){
      return(sessionStorage.getItem('token'))
    }
  }

  setToken(username, token, hash):void{
    if(this.remember==true){
      localStorage.clear();
      localStorage.setItem('username', username);
      localStorage.setItem('token', token);
      localStorage.setItem('hash', hash);
    }
    else{
      sessionStorage.clear();
      sessionStorage.setItem('username', username);
      sessionStorage.setItem('token', token);
      sessionStorage.setItem('hash', hash);
    }
  }
}
