import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthGuard } from './auth-guard.service';
import 'rxjs/add/operator/map';

interface ItemsResponse {
  status: Number;
  data: any;
  message: string;
}
@Injectable()
export class DataService {


  result:any;
  request:any;
  auth:any;
  test:any;
  apiRoute = "http://localhost:3000/api/";

  constructor(private _http: HttpClient,
              private authGuard: AuthGuard) {
  }

  setAuth(){
    this.auth=this.authGuard.getToken()
  }

  getUsers() {
    return this._http.get<ItemsResponse>(this.apiRoute+"users")
  }

  postUsers() {
    return this._http.post<ItemsResponse>(this.apiRoute+"register", this.request)
  }

  login() {
    return this._http.post<ItemsResponse>(this.apiRoute+"login", this.request)
  }

  editUser() {
    this.setAuth();
    return this._http.put<ItemsResponse>(this.apiRoute+"edit_user", this.request, {
      headers: new HttpHeaders().set('Authorization', this.auth),
    })

  }

  newPost() {
    this.setAuth();
    return this._http.post<ItemsResponse>(this.apiRoute+"new_post", this.request, {
    headers: new HttpHeaders().set('Authorization', this.auth),
    })
  }

  deletePost() {
    this.setAuth();
    return this._http.delete<ItemsResponse>(this.apiRoute+"delete_post/"+this.request,{
    headers: new HttpHeaders().set('Authorization', this.auth),
    })
  }

  editPost() {
    this.setAuth();
    return this._http.put<ItemsResponse>(this.apiRoute+"edit_post", this.request, {
      headers: new HttpHeaders().set('Authorization', this.auth),
    })
  }

  getPosts() {
    return this._http.get<ItemsResponse>(this.apiRoute + "get_posts/"+this.request)
  }

  getAllPosts() {
    return this._http.get<ItemsResponse>(this.apiRoute + "get_all_posts")
  }

  getLastPosts() {
    return this._http.get<ItemsResponse>(this.apiRoute + "get_last_posts")
  }

  getPostById() {
    return this._http.get<ItemsResponse>(this.apiRoute + "get_post_by_id/"+this.request)
  }

  follow() {
    return this._http.post<ItemsResponse>(this.apiRoute + "follow", this.request)
      // .map(data => this.result = data['results']);
  }

  unfollow() {
    return this._http.post<ItemsResponse>(this.apiRoute + "unfollow", this.request)
      // .map(data => this.result = data['results']);
  }

  block() {
    return this._http.post<ItemsResponse>(this.apiRoute + "block", this.request)
      // .map(data => this.result = data['results']);
  }

  unblock() {
    return this._http.post<ItemsResponse>(this.apiRoute + "unblock", this.request)
      // .map(data => this.result = data['results']);
  }

  getFollowedPosts() {
    return this._http.post<ItemsResponse>(this.apiRoute + "get_followed_post", this.request)
  }

  getPostsByHash() {
    return this._http.post<ItemsResponse>(this.apiRoute + "get_post_by_hash", this.request)
  }

  getCurrentUserPosts() {
    return this._http.post<ItemsResponse>(this.apiRoute + "get_current_user_posts", this.request)
  }

  getFollowers() {
    this.request = this.request.username
    return this._http.get<ItemsResponse>(this.apiRoute + "get_followers/"+this.request)
  }
}
