import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router} from '@angular/router'

@Component({
  selector: 'app-showposts',
  templateUrl: './showposts.component.html',
  styleUrls: ['./showposts.component.css', '../app.component.css']
})
export class ShowpostsComponent implements OnInit{
  posts: Array<any>;
  url: Array<any>;
  author: String;

  constructor(private _dataService: DataService,
              private router: Router) {
    if(!localStorage.getItem("username") && !sessionStorage.getItem('username')){
      this.router.navigate(['/login'])
    }

  }

  ngOnInit(){
    this.url = this.router.url.split("/")
    this.author = this.url.slice(2).toString();
    this._dataService.request  = this.author
    this._dataService.getPosts()
        .subscribe(res => {
          this.posts = res['data']
          console.log(this.posts)
        });
  }

}
