import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule }   from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { CookieService } from 'ngx-cookie-service'
import {HttpClientModule} from '@angular/common/http';
import { Ng4FilesModule } from '../../node_modules/angular4-files-upload';

import { DataService } from './data.service';
import { AuthGuard } from './auth-guard.service';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';
import { ShowpostsComponent } from './showposts/showposts.component';
import { FollowedComponent } from './followed/followed.component';
import { CurrentUserComponent } from './current-user/current-user.component';
import { EditPostComponent } from './edit-post/edit-post.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { FileUploadComponent } from './file-upload/file-upload.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UsersComponent,
    PostsComponent,
    ShowpostsComponent,
    FollowedComponent,
    CurrentUserComponent,
    EditPostComponent,
    UserDetailComponent,
    EditUserComponent,
    FileUploadComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    Ng4FilesModule,
    RouterModule.forRoot([
      { path: '',   redirectTo: '/login', pathMatch: 'full' },
      { path:'register', component: RegisterComponent },
      { path:'login', component: LoginComponent },
      { path:'logout', component: LoginComponent },
      { path:'users', component: UsersComponent, canActivate: [AuthGuard] },
      { path:'current_user', component: CurrentUserComponent, canActivate: [AuthGuard] },
      { path:'edit_user', component: EditUserComponent, canActivate: [AuthGuard] },
      { path:'show_posts/:author', component: ShowpostsComponent, canActivate: [AuthGuard] },
      { path:'new_post', component: PostsComponent, canActivate: [AuthGuard] },
      { path:'edit_post/:id', component: EditPostComponent, canActivate: [AuthGuard] },
      { path:'followed', component: FollowedComponent, canActivate: [AuthGuard] },
      { path:'detail', component: UserDetailComponent, canActivate: [AuthGuard] }
    ])
  ],
  providers: [DataService, CookieService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
