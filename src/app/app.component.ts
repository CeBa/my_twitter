import { Component } from '@angular/core';
import { DataService } from './data.service';
import { AuthGuard } from './auth-guard.service';
import { Subject } from 'rxjs/Subject';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  posts: Array<any>;
  username: String;
  public static updatePosts: Subject<boolean> = new Subject();
  public static updateUsername: Subject<boolean> = new Subject();
  public static updateHash: Subject<boolean> = new Subject();
  searchBar="";
  searchByHashtags="";
  err="";
  hash=""


  constructor(private _dataService: DataService,
              private cookieService: CookieService,
              private router: Router,
              private authGuard: AuthGuard){
    this._dataService.getLastPosts()
    .subscribe(res => this.posts = res.data);

    AppComponent.updatePosts.subscribe(res => {
      this._dataService.getLastPosts()
      .subscribe(res => this.posts = res.data);
    })

    AppComponent.updateUsername.subscribe(res => {
      this.username = this.authGuard.getCurrentUser();
    })
    AppComponent.updateHash.subscribe(res => {
      this.hash = this.authGuard.getHash();
    })
    this.username=this.authGuard.getCurrentUser();
    this.hash=this.authGuard.getHash();
  }
  logout():void{
    $("#result").empty();
    localStorage.clear();
    sessionStorage.clear();
    this.username=""
    this.router.navigate(['/login'])
    this.authGuard.url = "";
  }

  onSubmit() {
    if(this.searchBar!=""){
      this._dataService.request={hashtag:this.searchBar};
      this._dataService.getPostsByHash()
      .subscribe(res => {
        this.searchByHashtags = res.data;
        $("#result").empty();
        $("#result").append( `
          <h2>Results</h2>
            <table>
              <thead>
                <tr>
                  <th>Users</th>
                  <th>Twits</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody id="res_twits">

              </tbody>
            </table>
            <button type="button" id="stop">Quit</button>
          <script>
            $("#stop").click(function(){
              $("#result").empty();
            })
          </script>
        ` );
        res.data.forEach(function(twit){
          var date = new Date(twit.date); // had to remove the colon (:) after the T in order to make it work
          var day = date.getDate();
          var monthIndex = date.getMonth();
          var year = date.getFullYear();
          var minutes = date.getMinutes();
          var hours = date.getHours();
          var formatedDate = hours + "h" + minutes + " " + day + "/" + monthIndex + "/" + year;
          $("#res_twits").append("<tr><td>"+twit.author+"</td><td>"+twit.content+"</td><td>"+formatedDate+"</td></tr>")
        })
      },error => {
        $("#result").append(`
          <p>No twit found with this #</p>
          <button type="button" id="stop">Quit</button>
          <script>
            $("#stop").click(function(){
              $("#result").empty();
            })
          </script>
        `);
      });
    }
  }
}
