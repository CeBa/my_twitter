import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { AuthGuard } from '../auth-guard.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css', '../app.component.css']
})
export class UsersComponent implements OnInit{

  users: Array<any>;
  message: String;
  public static updateUsers: Subject<boolean> = new Subject();
  public currentUser = ""
  selectedUser: any;
  test1="test";
  blocked: Array<any>;

  constructor(private _dataService: DataService,
              private authGuard: AuthGuard,
              private router: Router) {
      this.currentUser = this.authGuard.getCurrentUser();
  }

  ngOnInit(){
    this._dataService.getUsers()
        .subscribe(res => {
          this.users = res.data;
          this.alpha();
          this.getBlocked();
        });

    UsersComponent.updateUsers.subscribe(res=>{
      this._dataService.getUsers()
      .subscribe(res => {
        this.users = res.data;
        var e = (document.getElementById("order")) as HTMLSelectElement;
        var sel = e.selectedIndex;
        var opt = e.options[sel];
        var CurValue = (opt).value;
        if(CurValue == "pop"){this.pop();}
        else if (CurValue == "prol"){this.prol()}
        else if (CurValue == "default"){this.alpha()}
        this.getBlocked();
      });
    })
  }

  getBlocked(){
    var thisUser = (this.users.find(function(user){
      return user.username == this;
    }, this.currentUser))
    this.blocked = thisUser.blocked;
  }

  follow(username):void{
    this._dataService.request = {username:username, follower:this.currentUser};
    this._dataService.follow()
        .subscribe(res => {
          this.message = res.message;
          this.appendMessage(this.message)
        }, error => {
          this.message = (error._body);
        });
    UsersComponent.updateUsers.next(true);
  }
  unfollow(username):void{
    this._dataService.request = {username:username, follower:this.currentUser};
    this._dataService.unfollow()
        .subscribe(res => {
          this.message = res.message;
          this.appendMessage(this.message)
        }, error => {
          this.message = (error._body);
        });
    UsersComponent.updateUsers.next(true);
  }
  block(username):void{
    this._dataService.request = {blocker:this.currentUser, blocked:username};
    this._dataService.block()
        .subscribe(res => {
          this.message = res.message;
          this.appendMessage(this.message)
        }, error => {
          this.message = (error._body);
        });
    UsersComponent.updateUsers.next(true);
  }
  unblock(username):void{
    this._dataService.request = {blocker:this.currentUser, blocked:username};
    this._dataService.unblock()
        .subscribe(res => {
          this.message = res.message
          this.appendMessage(this.message)
        }, error => {
          this.message = (error._body);
        });
    UsersComponent.updateUsers.next(true);
  }

  appendMessage(message):void{
    let div = document.getElementById('message') as HTMLSelectElement;
    div.innerHTML = "";
    div.innerHTML += message;
    setTimeout(function(div){
      div.innerHTML = "";
    }, 2000, div);
  }

  detailer(user){
    this.selectedUser = user;
  }

  pop():void{
    this.users = this.users.sort(function(a, b){return b.followedBy.length - a.followedBy.length});
  }

  prol():void{
    this.users = this.users.sort(function(a, b){return b.nbrTwits - a.nbrTwits});
  }

  alpha():void{
    this.users = this.users.sort(function(a, b){
      var usernameB =  b.username.toUpperCase()
      var usernameA = a.username.toUpperCase()
      return (usernameA < usernameB) ? -1: (usernameA > usernameB)? 1: 0;
    });
  }

}
