import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { AuthGuard } from '../auth-guard.service';
import { Post } from './post';
import { CookieService } from 'ngx-cookie-service';
import { AppComponent } from '../app.component';
import { Router } from '@angular/router';



@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css', '../../form.css', '../app.component.css']
})
export class PostsComponent {

  constructor(private _dataService: DataService,
              private authGuard: AuthGuard,
              private cookieService: CookieService,
              private app: AppComponent,
              private router: Router) {}
  model = new Post('');
  submitted = false;
  message: String;
  token : String;

  onSubmit() {
    if(!localStorage.getItem('token') && !sessionStorage.getItem('token')){
      this.message = "Only logged users can post a new twit"
    }
    else{
      this.submitted = true;
      this._dataService.request = [this.model, this.authGuard.getCurrentUser()]
      this._dataService.newPost()
      .subscribe();
      this.message = "New twit posted"
      AppComponent.updatePosts.next(true);
      var router = this.router
      setTimeout(function(router){
        router.navigate(['/current_user'])
      }, 2000, router)
    }
  }

  get diagnostic() { return JSON.stringify(this.model);}

}
