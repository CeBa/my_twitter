import { Component, OnInit } from '@angular/core';
// import {
//   Ng4FilesStatus,
//   Ng4FilesSelected
// } from '../../../node_modules/angular4-files-upload/src/app/ng4-files';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  public selectedFiles;

  constructor() { }
  // constructor(private ng4FilesService: Ng4FilesService) { }

  // private testConfig: Ng4FilesConfig = {
  //   acceptExtensions: ['jpeg', 'jpg'],
  //   maxFilesCount: 5,
  //   maxFileSize: 5120000,
  //   totalFilesSize: 10120000
  // };
  //
  // public filesSelect(selectedFiles: Ng4FilesSelected): void {
  //   if (selectedFiles.status !== Ng4FilesStatus.STATUS_SUCCESS) {
  //     this.selectedFiles = selectedFiles.status;
  //     return;
  //
  //     // Hnadle error statuses here
  //   }
  //
  //   this.selectedFiles = Array.from(selectedFiles.files).map(file => file.name);
  // }

  ngOnInit() {
    // this.ng4FilesService.addConfig(this.testConfig);
  }

}
