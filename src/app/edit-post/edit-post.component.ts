import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { AuthGuard } from '../auth-guard.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '../app.component';
import { CookieService } from 'ngx-cookie-service';
import { Post } from './post';


@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css', '../../form.css', '../app.component.css']
})
export class EditPostComponent implements OnInit {

  id = "";
  model = new Post('');
  submitted = false;
  message: String;
  token : String;
  content : String;
  currentUser = "";
  constructor(private activetedRoute: ActivatedRoute,
              private _dataService: DataService,
              private authGuard: AuthGuard,
              private app: AppComponent,
              private router: Router,
              private cookieService: CookieService) {
    this.activetedRoute.paramMap.subscribe( params =>{
      this.id = params.get('id');
    })
    this.currentUser = this.authGuard.getCurrentUser();
  }

  onSubmit() {
    this.submitted = true;
    this._dataService.request = {content:this.model, id:this.id}
    this._dataService.editPost()
    .subscribe();
    AppComponent.updatePosts.next(true);
    this.router.navigate(['/current_user'])
  }


  ngOnInit() {
    this._dataService.request = this.id;
    this._dataService.getPostById().subscribe(res =>{
      if(res['data'].author != this.currentUser){
        this.router.navigate(['/current_user']);
      }
      this.content = res['data'].content;
    })
  }

}
