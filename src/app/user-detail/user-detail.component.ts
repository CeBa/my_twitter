import { Component, Input, OnInit } from '@angular/core';
import { User } from '../login/user';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
})
export class UserDetailComponent implements OnInit {

  constructor() { }
  @Input() user:any;
  @Input() test:"";

  ngOnInit() {
    console.log (this.user);
  }

}
