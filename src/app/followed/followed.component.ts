import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { AuthGuard } from '../auth-guard.service';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';


@Component({
  selector: 'app-followed',
  templateUrl: './followed.component.html',
  styleUrls: ['./followed.component.css', '../app.component.css']
})
export class FollowedComponent implements OnInit {
  posts: Array<any>;
  allPosts = [];
  message = "";
  currentUser = "";

  constructor(private _dataService: DataService,
              private authGuard: AuthGuard,
              private router: Router) {
                this.currentUser = this.authGuard.getCurrentUser();
  }

  ngOnInit() {
    this._dataService.request = {username:this.currentUser};
    this._dataService.getFollowedPosts()
        .subscribe(res => {
          if(res.data.length == 0){
            this.message = res.message;
          }
          this.posts = res.data;
          for(let i=0; i < res.data.length; i++){
            for (let j=0; j < res.data[i].length; j++){
              this.allPosts.push(res.data[i][j]);
            }
          }
          this.allPosts = this.allPosts.sort(function(a, b){
            return new Date(b.date).getTime() - new Date(a.date).getTime();
          });
        },error => {
          console.log(error)
        });
  }

}
