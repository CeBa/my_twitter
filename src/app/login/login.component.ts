import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { User } from './user';
import { CookieService } from 'ngx-cookie-service';
import { AppComponent } from '../app.component';
import { Router, RouterStateSnapshot } from '@angular/router';
import { AuthGuard } from '../auth-guard.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', '../../form.css', '../app.component.css']
})
export class LoginComponent {

  constructor(private _dataService: DataService,
              private cookieService: CookieService,
              private app: AppComponent,
              private router:Router,
              private authGuard: AuthGuard) {
                if(localStorage.getItem('username') || sessionStorage.getItem('username')){
                  this.router.navigate(['/current_user'])
                }
              }

  model = new User('', '');
  submitted = false;
  message = "";
  remember = false;
  redirectUrl = "";

  onRemember(){
    if(this.remember == false){
      this.authGuard.remember = true;
    }
    else{
      this.authGuard.remember = false
    }
  }

  onSubmit() {
    this._dataService.request = this.model
    this._dataService.login()
        .subscribe(res => {
          if(res.message != "Success"){
            this.message = res.message;
          }
          else{
              this.submitted = true;
              this.authGuard.loggedIn = true;
              this.authGuard.setToken(this.model.username, res.data.token, res.data.hash)
              AppComponent.updateUsername.next(true);
              AppComponent.updateHash.next(true);
              if(this.authGuard.url){
                this.router.navigateByUrl(this.authGuard.url)
              }
              else{
                this.router.navigateByUrl("/current_user")
              }
            }
        },error => {
          console.log(error);
      });
  }
}
