import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../data.service';
import { Subject } from 'rxjs/Subject';
import { AuthGuard } from '../auth-guard.service';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
declare var jquery:any;
declare var $ :any;


@Component({
  selector: 'app-current-user',
  templateUrl: './current-user.component.html',
  styleUrls: ['./current-user.component.css', '../app.component.css']
})
export class CurrentUserComponent implements OnInit {

  posts: Array<any>;
  followers: Array<any>;
  followed: Array<any>;
  currentUser = "";
  followerMessage ="";
  followedMessage = "";
  postMessage = "";
  public static updatePosts: Subject<boolean> = new Subject();
  public static updateFollowers: Subject<boolean> = new Subject();

  constructor(private _dataService: DataService,
              private authGuard: AuthGuard,
              private app: AppComponent,
              private cookieService: CookieService,
              private router: Router) {
                this.currentUser = this.authGuard.getCurrentUser();
              }

  delete(id){
    this._dataService.request=id;
    this._dataService.deletePost()
    .subscribe(res=>{
      AppComponent.updatePosts.next(true);
      CurrentUserComponent.updatePosts.next(true);
    }, error=>{
      console.log(error);
    })
  }

  unfollow(username){
    this._dataService.request={username:username, follower:this.currentUser};
    this._dataService.unfollow()
    .subscribe(res=>{
      AppComponent.updatePosts.next(true);
      CurrentUserComponent.updateFollowers.next(true);
      $("#"+username+"followed").remove();
    }, error=>{
      console.log(error);
    })
  }

  ngOnInit() {
    this.getData();
    CurrentUserComponent.updatePosts.subscribe(res=>{
      this.getData();
    })
    CurrentUserComponent.updateFollowers.subscribe(res=>{
      this.getData();
    })
  }

  getData() {
    this._dataService.request={username:this.currentUser}
    this._dataService.getCurrentUserPosts()
    .subscribe(res=>{
      this.posts = res.data;
      if(this.posts.length == 0){
        this.postMessage = "You have posted nothing yet";
      }
    },error=>{
      console.log(error);
    })
    this._dataService.getFollowers()
    .subscribe(res=>{
      if(res.data.followedBy.length == 0){
        this.followerMessage = "Nobody is following you, loser"
      }
      if(res.data.follow.length == 0){
        this.followedMessage = "You're not following anyone"
      }
      this.followers = res.data.followedBy;
      this.followed = res.data.follow;
    },error=>{
      console.log(error);
    })

  }

}
