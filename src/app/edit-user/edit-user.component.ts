import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { AuthGuard } from '../auth-guard.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '../app.component';
import { CookieService } from 'ngx-cookie-service';
import { User } from './user';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css', '../../form.css', '../app.component.css']
})
export class EditUserComponent implements OnInit {

  model = new User('', '', '');
  submitted = false;
  conf_pass = "";
  message = "";
  currentUser: String;

  constructor(private _dataService: DataService,
              private authGuard: AuthGuard,
              private router: Router,
              private activetedRoute: ActivatedRoute,
              private app: AppComponent) {
              this.currentUser = this.authGuard.getCurrentUser() }

  onSubmit() {
    this._dataService.request = {newUser: this.model, prevUsername:this.currentUser}
    if(this.conf_pass == this.model.password){
      this._dataService.editUser()
      .subscribe(res => {
        this.message=res['message'];
        console.log(this.message);
        this.authGuard.setToken(this.model.username, res['data'].token, res['data'].hash)
        AppComponent.updateUsername.next(true);
        AppComponent.updatePosts.next(true);
        this.router.navigateByUrl("/current_user")
      });
    }
    else{
      this.message = "The password and its confirmation must be the same. "
    }
  }

  ngOnInit() {
  }

}
